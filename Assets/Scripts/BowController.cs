﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BowController : MonoBehaviour
{

    //farthest point the arrow can bend
    public Transform farthestPoint;
    public Transform playerGrabPoint;
    //Transform point of where the arrow will be placed
    public Transform arrowPoint;

    //Ropes of the bow, this is a two part rope
    public GameObject upperRope;
    public GameObject lowerRope;

    public Quaternion angleUpperRope;
    public Quaternion angleLowerRope; 

    //Variable of the hand and the arrow used on the bow
    public GameObject handPlayer;
    public GameObject arrow;
    
    public Vector3 forwardVectorBow;

    //Scalar product 
    public float scalarProduct;
    //Power mulitplier to enhance the shot of the arrow
    public float powerMultiplier;
    
    public bool isStretched = false;

    //transform.up * scalarAngle * PowerMultiplier
    public Vector3 pullForce;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (arrow != null)
        {
            //Get the scalar product between the vector handplayer to arrowPoint and farthestpoint to arrowPoint
            scalarProduct = Vector3.Dot(handPlayer.transform.position - arrowPoint.transform.position,
                (farthestPoint.transform.position - arrowPoint.transform.position).normalized);

            //To keep the arrow to stay on a reasonable distance between the bow and the farthest point
            scalarProduct = Mathf.Clamp(scalarProduct, 0, 1);

            //Move the ropes only if the scalar is between 0 and 1 (between the arrowPoint and the farthestpoint)
            if (scalarProduct >=  0 && scalarProduct <= 1)
            {
                //Get the angle between the " vector of highest point of the upperRope and the Arrow point" and the "vector of highest point of the upperRope and the hand position
                //It's actually not used in the program but we used it in the beginning of the project, so we wanted to show it
                float angle = Vector3.Angle(upperRope.transform.localPosition - arrow.transform.localPosition, upperRope.transform.localPosition - arrowPoint.transform.localPosition);
                

                //Stock the vector in a variable that will be used afterward
                pullForce = transform.up * scalarProduct * powerMultiplier;
                
                if (Input.GetButton("Fire1"))
                {
                    //Move the arrow between the arrowPoint and the farthestPoint
                    arrow.transform.position = transform.position - transform.up * scalarProduct;
                
                    //Update the rotation of the 2 ropes parts (It was a try to do it with the trigo system, but it ended not working)
                    //upperRope.transform.rotation = Quaternion.Euler(angleUpperRope + angle,0,0);
                    //lowerRope.transform.rotation = Quaternion.Euler(angleLowerRope - angle,0,0);
                    
                    upperRope.transform.LookAt(arrow.transform.position);
                    lowerRope.transform.LookAt(arrow.transform.position);
                    isStretched = true;
                }
                //Fire system, if the arrow is set, the player can click on his mouse to bend de ropes
                //If it release is finger, the arrow is landed
                else if(Input.GetButtonUp("Fire1"))
                {
                    isStretched = false;
                    StartCoroutine(DisableCollider(0.5f));
                    ResetRopeBow();
                    FireArrow();
                }
            }
        }
        else
        {
            //Get the local rotation.x of the upper and lower rope.
            //they are used to reset the ropes rotation
            angleUpperRope = upperRope.transform.rotation;
            angleLowerRope = lowerRope.transform.rotation;
        }
    }

    //Fix the arrow on the hand.
    //Rotate the same way the hand rotate
    public void SetArrowAndHand(GameObject arrow)
    {
        arrow.transform.parent = gameObject.transform;
        arrow.transform.position = gameObject.transform.position;
        this.arrow = arrow;
        
        Debug.Log("Angle = " + Vector3.Angle(transform.up,arrow.transform.forward));
        Debug.Log("AngleCos = " + Vector3.Angle(transform.up,arrow.transform.forward));
        
        float angle = Vector3.Angle(transform.up, arrow.transform.forward);
        arrow.transform.rotation = Quaternion.Euler(arrow.transform.rotation.x - angle, arrow.transform.rotation.y,
            arrow.transform.rotation.z);
    }

    //Reset the rotation of both ropes
    public void ResetRopeBow()
    {
        upperRope.transform.rotation = angleUpperRope;
        lowerRope.transform.rotation = angleLowerRope;
    }


    //Function that will land the arrow. 
    public void FireArrow()
    {
        ArrowController arrowToFire = arrow.GetComponent<ArrowController>();
        arrowToFire.onBow = false;
        arrow.transform.SetParent(null);
        arrow = null;
        arrowToFire.handTransform = null;
        arrowToFire.rgbd.useGravity = true;
        
        arrowToFire.rgbd.AddForce(pullForce,ForceMode.Impulse);
        
    }

    //Disable the collider in order to make the arrow not getting collided with the bow during the shoot
    public IEnumerator DisableCollider(float time)
    {
        arrowPoint.gameObject.GetComponent<BoxCollider>().enabled = false;
        yield return new WaitForSeconds(time);
        arrowPoint.gameObject.GetComponent<BoxCollider>().enabled = true;
    }
}