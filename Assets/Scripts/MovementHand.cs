﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementHand : MonoBehaviour
{
    public float rotationSpeed = 5f;
    public float moveSpeed = 5f;

    public GameObject bow;
    public GameObject rightHand;

    private Transform bowInitialTransform;
    private Transform handInitialTransform;
    
    private bool isShiftKeyDown = false;

    void Start()
    {
        if (bow != null)
        {
            bowInitialTransform = bow.transform;
        }
        
        if (rightHand != null)
        {
            handInitialTransform = rightHand.transform;
        }
    }

    // Update is called once per frame
    void Update()
    {
        isShiftKeyDown = IsShiftKeyDown();
        Rotation();
        Move();
        ResetPosition();
    }

    void Rotation()
    {
        // Rotation of the bow
        #region RotationBow
        if (bow != null && isShiftKeyDown)
        {
            if (Input.GetKey(KeyCode.Z))
            {
                // rotation up
                bow.transform.Rotate(Vector3.left * Time.deltaTime * rotationSpeed, Space.Self);
            }

            if (Input.GetKey(KeyCode.S))
            {
                // rotation down
                bow.transform.Rotate(Vector3.right * Time.deltaTime * rotationSpeed, Space.Self);
            }

            if (Input.GetKey(KeyCode.D))
            {
                // rotation right
                bow.transform.Rotate(Vector3.back * Time.deltaTime * rotationSpeed, Space.Self);
            }

            if (Input.GetKey(KeyCode.Q))
            {
                // rotation left
                bow.transform.Rotate(Vector3.forward * Time.deltaTime * rotationSpeed, Space.Self);
            }
            
            if (Input.GetKey(KeyCode.E))
            {
                // Lean to the right
                bow.transform.Rotate(Vector3.down * Time.deltaTime * rotationSpeed, Space.Self);
            }

            if (Input.GetKey(KeyCode.A))
            {
                // Lean to the left
                bow.transform.Rotate(Vector3.up * Time.deltaTime * rotationSpeed, Space.Self);
            }
        }
        #endregion

        // Rotatiion of the right hand
        #region RotationRightHand
        if (rightHand != null && isShiftKeyDown)
        {
            if (Input.GetKey(KeyCode.I))
            {
                // rotation up
                rightHand.transform.Rotate(Vector3.left * Time.deltaTime * rotationSpeed, Space.Self);
            }

            if (Input.GetKey(KeyCode.K))
            {
                // rotation down
                rightHand.transform.Rotate(Vector3.right * Time.deltaTime * rotationSpeed, Space.Self);
            }

            if (Input.GetKey(KeyCode.L))
            {
                // rotation right
                rightHand.transform.Rotate(Vector3.up * Time.deltaTime * rotationSpeed, Space.Self);
            }

            if (Input.GetKey(KeyCode.J))
            {
                // rotation left
                rightHand.transform.Rotate(Vector3.down * Time.deltaTime * rotationSpeed, Space.Self);
            }
            
            if (Input.GetKey(KeyCode.O))
            {
                // Lean to the right
                rightHand.transform.Rotate(Vector3.back * Time.deltaTime * rotationSpeed, Space.Self);
            }

            if (Input.GetKey(KeyCode.U))
            {
                // Lean to the left
                rightHand.transform.Rotate(Vector3.forward * Time.deltaTime * rotationSpeed, Space.Self);
            }
        }
        #endregion
    }

    void Move()
    {
        // Movement of the bow
        #region MovementBow
        if (bow != null && !isShiftKeyDown)
        {
            if (Input.GetKey(KeyCode.Z))
            {
                // move forward
                // bow.transform.Translate(Vector3.forward * Time.deltaTime * moveSpeed);
                bow.transform.Translate(Vector3.up * Time.deltaTime * moveSpeed);
            }

            if (Input.GetKey(KeyCode.S))
            {
                // move backward
                // bow.transform.Translate(Vector3.back * Time.deltaTime * moveSpeed);
                bow.transform.Translate(Vector3.down * Time.deltaTime * moveSpeed);
            }

            if (Input.GetKey(KeyCode.D))
            {
                // move to the right
                bow.transform.Translate(Vector3.right * Time.deltaTime * moveSpeed);
            }

            if (Input.GetKey(KeyCode.Q))
            {
                // move to the left
                bow.transform.Translate(Vector3.left * Time.deltaTime * moveSpeed);
            }
            
            if (Input.GetKey(KeyCode.A))
            {
                // move up
                // bow.transform.Translate(Vector3.up * Time.deltaTime * moveSpeed);
                bow.transform.Translate(Vector3.forward * Time.deltaTime * moveSpeed);
            }
            
            if (Input.GetKey(KeyCode.E))
            {
                // move down
                // bow.transform.Translate(Vector3.down * Time.deltaTime * moveSpeed);
                bow.transform.Translate(Vector3.back * Time.deltaTime * moveSpeed);
            }
            
            
        }
        #endregion

        // Movement of the right hand
        #region MovementRightHand
        if (rightHand != null && !isShiftKeyDown)
        {
            if (Input.GetKey(KeyCode.I))
            {
                // move forward
                rightHand.transform.Translate(Vector3.forward * Time.deltaTime * moveSpeed);
            }

            if (Input.GetKey(KeyCode.K))
            {
                // move backward
                rightHand.transform.Translate(Vector3.back * Time.deltaTime * moveSpeed);
            }

            if (Input.GetKey(KeyCode.L))
            {
                // move to the right
                rightHand.transform.Translate(Vector3.right * Time.deltaTime * moveSpeed);
            }

            if (Input.GetKey(KeyCode.J))
            {
                // move to the left
                rightHand.transform.Translate(Vector3.left * Time.deltaTime * moveSpeed);
            }
            
            if (Input.GetKey(KeyCode.O))
            {
                // move up
                rightHand.transform.Translate(Vector3.up * Time.deltaTime * moveSpeed);
            }
            
            if (Input.GetKey(KeyCode.U))
            {
                // move down
                rightHand.transform.Translate(Vector3.down * Time.deltaTime * moveSpeed);
            }
        }
        #endregion
    }

    void ResetPosition()
    {
        // Press space : Reset to the initial position 
        if (Input.GetKey(KeyCode.Space))
        {
            if (bow != null)
            {
                bow.transform.position = bowInitialTransform.position;
                bow.transform.rotation = new Quaternion();
            }
            
            if (rightHand != null)
            {
                rightHand.transform.position = handInitialTransform.position;
                rightHand.transform.rotation = new Quaternion();
            }
        }
    }

    bool IsShiftKeyDown()
    {
        if (Input.GetKey(KeyCode.LeftShift))
        {
            return true;
        }
        return false;
    }
}