﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using UnityEngine;
using UnityEngine.SceneManagement;
using Quaternion = UnityEngine.Quaternion;
using Vector3 = UnityEngine.Vector3;

public class ArrowTrajectory : MonoBehaviour
{
    public GameObject bow;

    public float force;

    // Representation of the trajectory
    public GameObject pointPrefab;
    public GameObject[] points;
    // Number of preafabs to display
    public int pointsNumber;
    public BowController bowController;
    // Empty gameObject at the end of the arrow
    public GameObject arrowPoint;

    // Vector 0
    public Vector3 v0;
    public float alphaAngle;

    [SerializeField] private WindController _windController;

    // Start is called before the first frame update
    void Start()
    {
        // New array of GameObject
        points = new GameObject[pointsNumber];

        // Filling red points
        for (int i = 0; i < pointsNumber; i++)
        {
            points[i] = Instantiate(pointPrefab, arrowPoint.transform.position, Quaternion.identity);
        }
    }

    // Update is called once per frame
    void Update()
    {
        // Alpha angle
        alphaAngle = -bow.transform.eulerAngles.x;

        // Calculating V0
        v0 = bowController.pullForce;
        

        if (bowController.isStretched)
        {
            // Test de calcule de la vitesse en tout point
            //Debug.Log("Vx(t) = " + SpeedPointX().x);
            //Debug.Log("Vz(t) = " + SpeedPointZ(i * 0.1f).z);

            // points[i].transform.position = PointPositionX(i * 0.1f);
            // points[i].transform.position = PointPositionZ(i * 0.1f);


            for (int i = 0; i < pointsNumber; i++)
            {
                // Update the position of each point
                points[i].transform.position = PointPosition(i * 0.1f);


                // If bow is streched, active each point
                if (bowController.isStretched)
                {
                    points[i].SetActive(true);
                }
                else
                {
                    points[i].SetActive(false);
                }
            }
        }

        // Calculates speed at any point x
        Vector3 SpeedPointX()
        {
            Vector3 pointX = v0 * Mathf.Cos(alphaAngle);
            return pointX;
        }

        // Calculates speed at any point z
        Vector3 SpeedPointZ(float time)
        {
            Vector3 pointZ = -Physics.gravity * time + v0 * Mathf.Sin(alphaAngle);
            return pointZ;
        }

        // Z(t) = -1/2 * g * t² + V0 * Sin(alpha) * t + Cz
        Vector3 PointPosition(float time)
        {
            Vector3 currentPointPos;

            //Test 
            //Here we tried to use the original method, but it ended not working.
            float x = (v0.x * Mathf.Cos(alphaAngle) * time);
            Debug.Log(x);

            float z = (v0.z * Mathf.Sin(alphaAngle) * time) + 0.5f * -Physics.gravity.magnitude * (time * time);
            Debug.Log(z);


            currentPointPos = new Vector3(x, 0, z);

            //Wind calculation
            if (_windController.onWind)
            {
                //Add a wind force over time (In addition of the gravity)
                currentPointPos =
                    transform.position + (v0 * force * time) + 0.5f * Physics.gravity * (time * time) +
                    0.5f * _windController.windDirection * _windController.windPower * (time * time);
            }
            else
            {
                //Add only the gravity factor
                currentPointPos =
                    transform.position + (v0 * force * time) + 0.5f * Physics.gravity * (time * time);
            }

            return currentPointPos;
        }
    }
}
