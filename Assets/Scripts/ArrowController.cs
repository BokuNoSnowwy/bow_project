﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowController : MonoBehaviour
{
    public Transform handTransform;
    public ArrowSpawner arrowSpawner;
    public GameObject arrowTransform;
    
    public bool onBow;

    public Rigidbody rgbd;

    private WindController _windController;
    // Start is called before the first frame update

    private void Awake()
    {
        _windController = FindObjectOfType<WindController>();
        arrowSpawner = FindObjectOfType<ArrowSpawner>();
        rgbd = GetComponent<Rigidbody>();
        arrowSpawner.ArrowDespawn(gameObject);
    }

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (handTransform != null && !onBow)
        {
            transform.position = handTransform.position;
        }
        
        
        //Make the arrow look realistic in the air. 
        //Use the rotation on the rgbd to modify the actual arrow rotation
        if (rgbd.velocity != Vector3.zero)
        {
            rgbd.rotation = Quaternion.LookRotation(rgbd.velocity);
            
            //Add the wind direction to the arrow over time
            if (_windController.onWind)
            {
                //5f is an offset, is not that powerfull otherwise
                rgbd.AddForce(_windController.windDirection * _windController.windPower * (Time.deltaTime) * 5f);
            }
        }
    }

    //If the arrow touch the hand, put the hand transform in the handTransform variable
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Hand"))
        {
            handTransform = other.gameObject.transform;
        }

        
        if (other.gameObject.CompareTag("ArrowPoint"))
        {
            if (!onBow)
            {
                other.GetComponentInParent<BowController>().SetArrowAndHand(gameObject);
                transform.position = other.GetComponentInParent<BowController>().arrowPoint.transform.position;
            }
            onBow = true;
        }

        //If the arrow thouch the floor, it's restacked in the spawner and the next arrow spawn.
        if (other.gameObject.layer == LayerMask.NameToLayer("Sol"))
        {
            gameObject.transform.parent = arrowTransform.transform;
            arrowSpawner.ArrowNext();
            arrowSpawner.ArrowDespawn(gameObject);
        }
    }
    
    
}
