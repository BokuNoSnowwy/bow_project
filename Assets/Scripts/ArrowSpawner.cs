﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowSpawner : MonoBehaviour
{
    public GameObject[] nbArrowToStock;
    public Stack<GameObject> goStack = new Stack<GameObject>();
    // Start is called before the first frame update
    void Start()
    {
        ArrowNext();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    //Function that will push the arrow in the Stack and make it disappear
    public void ArrowDespawn(GameObject arrow)
    {
        goStack.Push(arrow);
        arrow.SetActive(false);
    }

    //Make the rotation of the stocked arrows, 
    public void ArrowNext()
    {
        ArrowController arrow = goStack.Pop().GetComponent<ArrowController>();
        arrow.gameObject.SetActive(true);
        arrow.gameObject.transform.position = transform.position;
        arrow.rgbd.useGravity = false;
        arrow.rgbd.velocity = Vector3.zero;

    }
}
