﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindController : MonoBehaviour
{
    
    [Header("Wind Management")]
    public bool onWind;
    public Vector3 windDirection;
    public float windPower;
    
}
